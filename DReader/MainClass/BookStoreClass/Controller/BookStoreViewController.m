//
//  BookStoreViewController.m
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "BookStoreViewController.h"
#import "GDSideViewController.h"
#import "SearchViewController.h"
@interface BookStoreViewController ()

@end

@implementation BookStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addNavigationControl];
    
}
#pragma mark - 添加导航控件
- (void)addNavigationControl {
    [self addPersonalButton];
    WeakSelf(weakSelf)
    [self.navBgView addNavigationSearchButton_Action:^(UIButton *button) {
        SearchViewController *searchVC = [[SearchViewController alloc] init];
        [weakSelf pushViewController:searchVC];
    }];
//    NSString *docsdir = [NSSearchPathForDirectoriesInDomains( NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
//    GDLog(docsdir)

   
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    AppSideVC.needSwipeShowMenu = YES;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
