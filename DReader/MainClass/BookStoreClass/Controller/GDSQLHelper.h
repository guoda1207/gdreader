//
//  GDSQLHelper.h
//  DReader
//
//  Created by moqing on 2018/2/5.
//  Copyright © 2018年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
@interface GDSQLHelper : NSObject

@property (nonatomic, retain, readonly) FMDatabaseQueue *dbQueue;

+ (instancetype)shareInstance;

+ (NSString *)dbPath;



@end
