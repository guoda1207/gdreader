//
//  GDSQLHelper.m
//  DReader
//
//  Created by moqing on 2018/2/5.
//  Copyright © 2018年 guoda. All rights reserved.
//

#import "GDSQLHelper.h"

@interface GDSQLHelper ()

@property (nonatomic, retain) FMDatabaseQueue *dbQueue;


@end

@implementation GDSQLHelper
static GDSQLHelper *_sqlManager = nil;
+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sqlManager = [[GDSQLHelper alloc] init];
    });
    return _sqlManager;
}
+ (NSString *) dbPathWithDirectoryName:(NSString*)directoryName {
    NSString *docsdir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    NSFileManager *filemanager = [NSFileManager defaultManager];
    if (directoryName == nil || directoryName.length == 0) {
        docsdir = [docsdir stringByAppendingPathComponent:@"SQL"];
    }else {
        docsdir = [docsdir stringByAppendingPathComponent:directoryName];
    }
    BOOL isDir;
    BOOL exit = [filemanager fileExistsAtPath:docsdir isDirectory:&isDir];
    if (!exit || !isDir) {
        [filemanager createDirectoryAtPath:docsdir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *dbPath = [docsdir stringByAppendingPathComponent:@"gddb.sqlite"];
    return dbPath;
}
+ (NSString *)dbPath {
    return [self dbPathWithDirectoryName:nil];
}

@end
