//
//  GDListViewController.m
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDListViewController.h"
#import "GDHeader.h"
#import "GDListLoginView.h"
#import "GDListHeaderView.h"
#import "GDLoginVC.h"
#import "GDSideViewController.h"
#import "GDListManager.h"
@implementation GDListViewController (gdlist)


@end


@interface GDListViewController ()

@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) GDListLoginView *loginHeaderView;

@property (nonatomic, strong) GDListHeaderView *UserHeaderView;


@end

@implementation GDListViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH/5*4, 160)];
    _headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_headerView];
    
    [self checkHeaderView];
}
- (void)checkHeaderView {
    _loginHeaderView = [[GDListLoginView alloc] initWithFrame:_headerView.bounds];
    WeakSelf(weakSelf)
    _loginHeaderView.listLoginBtnClick = ^(UIButton *sender) {
        
//        [weakSelf centerTouchClick:CenterPushType_Login];
//        UINavigationController *nav = (UINavigationController*)AppDelegate.window.rootViewController;
        GDLoginVC *login = [[GDLoginVC alloc] init];
        [weakSelf.navigationController pushViewController:login animated:YES];
        
//        GDSideViewController *side = [nav.viewControllers firstObject];
//        [side hideSideViewController:YES];
//        GDLog(@"去登陆 ");
    };
    [_headerView addSubview:_loginHeaderView];
    
//    _UserHeaderView = [[GDListHeaderView alloc] initWithFrame:_headerView.bounds];
//    _UserHeaderView.listHeaderClick = ^(UIButton *sender) {
//        GDLog(@"查看用户账号");
//    };
//    [_headerView addSubview:_UserHeaderView];
    
}
- (void)centerTouchClick:(CenterPushType)type {
    if ([GDListManager shareInstance].listManagerBlock){
        [GDListManager shareInstance].listManagerBlock(type);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


