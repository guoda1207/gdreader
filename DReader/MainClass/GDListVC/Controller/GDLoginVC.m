//
//  GDLoginVC.m
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDLoginVC.h"
#import "GDLoginRootView.h"
@interface GDLoginVC ()

@end

@implementation GDLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setBackNavigationButton];

    self.title = @"会员登录";
    
    GDLoginRootView *loginView = [[GDLoginRootView alloc] initWithFrame:CGRectMake(0, 64, SCREENWIDTH, SCREENHEIGHT-64)];
    [self.view addSubview:loginView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    GDLog(@"会员登录 dealloc");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
