//
//  GDListHeaderView.m
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDListHeaderView.h"
#import "GDHeader.h"
@interface GDListHeaderView ()

@property (nonatomic, strong) UIImageView *bacImageView;

@property (nonatomic, strong) UIButton *headerBtn;

@property (nonatomic, strong) UILabel  *userNameLaebl;

@property (nonatomic, strong) UIImageView *vipImg;

@property (nonatomic, strong) UILabel *vipLabel;


@end

@implementation GDListHeaderView

-(void)dealloc {
    GDLog(@"listheader dealloc");
}
- (instancetype)initWithFrame:(CGRect)frame {
    self =  [super initWithFrame:frame];
    if (self) {
        [self createListheaderView];
    }
    return self;
}

- (void)createListheaderView {
    
    _bacImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _bacImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_list_header",targetImageName]];
    [self addSubview:_bacImageView];
    //    __weak typeof(self) weakSelf = self;
    WeakSelf(weakSelf)
    _headerBtn = [self addSystemButtonWithFrame:CGRectMake(0, 0, 73, 73) title:nil action:^(UIButton *button) {
        if (weakSelf.listHeaderClick) {
            weakSelf.listHeaderClick(button);
        }
    }];
    _headerBtn.center = CGPointMake(SCREENWIDTH/5*4/2, 73/2+41);
    _headerBtn.layer.cornerRadius = 73/2;
    _headerBtn.clipsToBounds = YES;
    _headerBtn.backgroundColor = [UIColor grayColor];
    
    
    _userNameLaebl = [[UILabel alloc] initWithFrame:CGRectZero];
    [self addSubview:_userNameLaebl];
    [_userNameLaebl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerBtn.mas_bottom).offset(10);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.mas_equalTo(20);
    }];
    _userNameLaebl.textAlignment = NSTextAlignmentCenter;
    _userNameLaebl.text = @"123";
    
    
    _vipLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self addSubview:_vipLabel];
    [_vipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userNameLaebl.mas_bottom).offset(5);
        make.left.mas_equalTo((self.w - 80)/2);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];
    _vipLabel.textAlignment = NSTextAlignmentRight;
    _vipLabel.text = @"包年VIP";
    _vipLabel.font = [UIFont systemFontOfSize:14];
    _vipImg = [[UIImageView alloc] initWithFrame:CGRectZero];
    [_vipLabel addSubview:_vipImg];
    [_vipImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_vipLabel.mas_top);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.left.mas_equalTo(0);
    }];
    _vipImg.image = GDImage(@"list_vip");
    
}



@end
