//
//  GDListLoginView.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GDListLoginView : UIView

@property (nonatomic, copy) void(^listLoginBtnClick)(UIButton *sender);

@end
