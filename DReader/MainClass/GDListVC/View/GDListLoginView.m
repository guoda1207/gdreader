//
//  GDListLoginView.m
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDListLoginView.h"
#import "GDHeader.h"
@interface GDListLoginView ()

@property (nonatomic, strong) UIImageView *bacImageView;

@property (nonatomic, strong) UIButton *loginBtn;


@end
@implementation GDListLoginView
-(void)dealloc {
    GDLog(@"listlogin dealloc");
}
- (instancetype)initWithFrame:(CGRect)frame {
    self =  [super initWithFrame:frame];
    if (self) {
        [self createListLoginView];
    }
    return self;
}

- (void)createListLoginView {
    
    _bacImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _bacImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_list_header",targetImageName]];
    [self addSubview:_bacImageView];
//    __weak typeof(self) weakSelf = self;
    WeakSelf(weakSelf)
    _loginBtn = [self addSystemButtonWithFrame:CGRectMake(0, 0, 80, 30) title:@"点击登录" action:^(UIButton *button) {
        if (weakSelf.listLoginBtnClick) {
            weakSelf.listLoginBtnClick(button);
        }
    }];
    [_loginBtn setTitleColor:[UIColor colorWithRed:129.0/255.0 green:84.0/255 blue:54.0/255 alpha:1] forState:UIControlStateNormal];
    [_loginBtn setTitleColor:[UIColor colorWithRed:129.0/255.0 green:84.0/255 blue:54.0/255 alpha:0.7] forState:UIControlStateHighlighted];
    _loginBtn.backgroundColor = [UIColor colorWithRed:253.0/255 green:176.0/255 blue:15.0/255 alpha:1];
    
    _loginBtn.center = self.center;
    
}

@end
