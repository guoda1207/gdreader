//
//  GDLoginRootView.h
//  DReader
//
//  Created by moqing on 2017/9/5.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GDLoginRootView : UIView

@property (nonatomic, copy) void(^QQLoginBtn)(UIButton *sender);

@property (nonatomic, copy) void(^WeiXinLoginBtn)(UIButton *sender);

@property (nonatomic, copy) void(^PhoneLoginBtn)(UIButton *sender);

@end
