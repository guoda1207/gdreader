//
//  GDLoginRootView.m
//  DReader
//
//  Created by moqing on 2017/9/5.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDLoginRootView.h"
#import "GDHeader.h"
@implementation GDLoginRootView

//login_userIcon   login_qqbtn


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self AddloginRootView];
        self.backgroundColor = GDColor(0xf6f6f6, 1);
    }
    return self;
}
- (void)AddloginRootView {
    CGFloat scale = SCREENWIDTH / 375;
    
    UIImageView *headerImage = [[UIImageView alloc] initWithFrame:CGRectMake((SCREENWIDTH - 120)/2, 75*scale, 120, 120)];
    headerImage.image = GDImage(@"login_userIcon");
    [self addSubview:headerImage];
    
//    21 21  30
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, headerImage.max_Y + 21, self.w, 21)];
    titleLabel.text =@"第三方帐号登录";
    titleLabel.font = GD_Font(15*scale);
    titleLabel.textColor = [UIColor lightGrayColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];

    UIButton *weixinLogin = [self addImageButtonWithFrame:CGRectZero title:nil Image:nil backgroud:@"login_weixinbtn" action:^(UIButton *button) {
        
    }];
    [weixinLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(30);
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(315*scale);
        make.height.mas_equalTo(46*scale);
    }];
    UIButton *qqLogin = [self addImageButtonWithFrame:CGRectZero title:nil Image:nil backgroud:@"login_qqbtn" action:^(UIButton *button) {
        
    }];
    [qqLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weixinLogin.mas_bottom).offset(20);
        make.centerX.equalTo(weixinLogin.mas_centerX);
        make.width.equalTo(weixinLogin.mas_width);
        make.height.equalTo(weixinLogin.mas_height);
    }];
    
    UILabel *messageLabel = [self addLabelWithFrame:CGRectZero text:@"注：QQ帐号和微信帐号是两个独立帐号，帐号内阅读币不可通用。"];
    messageLabel.font = GD_Font(13);
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.textColor = [UIColor lightGrayColor];
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(qqLogin.mas_bottom).offset(10);
        make.centerX.equalTo(weixinLogin.mas_centerX);
        make.width.equalTo(weixinLogin.mas_width).offset(-10);
        make.height.mas_equalTo(42);
    }];
    
    UIButton *phoneLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:phoneLogin];
    [phoneLogin setBackgroundImage:[GDOperationManager createImageWithColor:mainColor] forState:UIControlStateNormal];
    phoneLogin.layer.cornerRadius = 6;
    phoneLogin.clipsToBounds = YES;
    [phoneLogin setTitle:@"手机号登录" forState:UIControlStateNormal];
    [phoneLogin addTarget:self action:@selector(phoneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [phoneLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(messageLabel.mas_bottom).offset(30);
        make.centerX.equalTo(weixinLogin.mas_centerX);
        make.width.equalTo(weixinLogin.mas_width);
        make.height.equalTo(weixinLogin.mas_height);
    }];
}
- (void)phoneBtnClick:(UIButton*)sender {

}
@end
