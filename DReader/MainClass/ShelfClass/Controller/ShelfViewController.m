//
//  ShelfViewController.m
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "ShelfViewController.h"
#import "GDSideViewController.h"
#import "GDListManager.h"
#import "GDLoginVC.h"

@interface ShelfViewController ()

@end

@implementation ShelfViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor grayColor];
//    [self personal_TouchEvent];
    
    
    [self addPersonalButton];
}

#pragma mark - 个人中心事件
- (void)personal_TouchEvent {
    WeakSelf(weakSelf)
    [GDListManager shareInstance].listManagerBlock = ^(CenterPushType type){
        [weakSelf pushListViewController:type];
    };
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    AppSideVC.needSwipeShowMenu = YES;

}
- (void)pushListViewController:(CenterPushType )type{
    //    [nav pushViewController:vc animated:YES];
    UIViewController *vc = [[UIViewController alloc] init];
    switch (type) {
        case CenterPushType_Login:
        {
            vc = [[GDLoginVC alloc] init];
            
        }
            break;
            
        default:
            break;
            
    }
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    [self hide_Or_appearSideVC:NO];

}
- (void)hide_Or_appearSideVC:(BOOL)isAppear {
//    GDSideViewController *side = AppSideVC;
//    if (isAppear)[side showLeftViewController:YES];
//    else [side hideSideViewController:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end




