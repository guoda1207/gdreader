//
//  TopicViewController.m
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "TopicViewController.h"
#import "GDHeader.h"
#import "GDSideViewController.h"
#import "SearchViewController.h"
#import "GDSQLHandle.h"
#import "GDFMDB.h"
#import "TestModel.h"
@interface TopicViewController ()


@end

@implementation TopicViewController

- (void)viewDidLoad {
    self.urlString = foundHttpURL;
    [super viewDidLoad];
    self.gdwebView.frame = CGRectMake(0, 64, SCREENWIDTH, SCREENHEIGHT-64 - 49);
    [self addTopicVCNavigation];
    
//    NSDictionary *dic = @{@"1":@"a",@"2":@"a",@"3":@"a",@"4":@"a",@"5":@"a"};
//    GDLog(@"%@",dic);
//    GDLog(@"%@",dic.allKeys);
//    for (NSString*string in dic) {
//        GDLog(@"%@",string);
//    }
//    [[GDFMDB shareInstance] jq_createTable:@"wohao" dicOrModel:dic excludeName:nil];
//
    TestModel *model = [[TestModel alloc] init];
    NSString *str = [[GDSQLHandle shareInstance] createTable:@"nihao" model:model excludeName:nil];
    GDLog(@"%@",str)
}

- (void)addTopicVCNavigation {
//    [self addPersonalButton];
    WeakSelf(weakSelf)
    UIView *searchView = [self.view addNavigationSearchButton_Action:^(UIButton *button) {
        SearchViewController *searchVC = [[SearchViewController alloc] init];
        [weakSelf pushViewController:searchVC];
    }];
    self.navigationItem.titleView = searchView;

    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    AppSideVC.needSwipeShowMenu = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
