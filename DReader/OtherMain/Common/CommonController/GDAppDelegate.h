//
//  GDAppDelegate.h
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GDSideViewController.h"

@interface GDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) GDSideViewController *sideViewController;

@end

