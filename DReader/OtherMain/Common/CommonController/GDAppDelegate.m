//
//  AppDelegate.m
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDAppDelegate.h"
#import "GDListViewController.h"
#import "GDTabbarVC.h"
#import "GDHeader.h"
#import "LocalSavePath.h"
@interface GDAppDelegate ()

@end

@implementation GDAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    application.statusBarStyle = UIStatusBarStyleLightContent;

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    
    GDTabbarVC *tabbar = [[GDTabbarVC alloc] init];
    
//    GDListViewController *list = [[GDListViewController alloc] init];
    
//    _sideViewController = [[GDSideViewController alloc] init];
//    [_sideViewController setLeftViewShowWidth:SCREENWIDTH/5*4];
//    [_sideViewController setLeftViewController:list];
//    [_sideViewController setRootViewController:tabbar];
//    _sideViewController.showBoundsShadow = YES;
    self.window.rootViewController = tabbar;
    //    _sideViewController.navigationController.navigationBarHidden = YES;

    [[LocalSavePath shareInstance] localSavePath_createPath];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
