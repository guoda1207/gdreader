//
//  GDRootViewController.h
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GDHeader.h"

@interface GDRootViewController : UIViewController


/**
 导航栏view
 */
@property (nonatomic, strong) UIView *navBgView;



- (void)pushViewController:(UIViewController*)viewController;

#pragma Mark - 左1按钮
- (void)setBackNavigationButton;
- (void)setLeftButtonWithImage:(NSString*)image;
- (void)rootLeftBtnClick:(UIButton*)sender;

#pragma Mark - 右1按钮
- (void)setRightButtonWithImage:(NSString*)image;
- (void)rootRightBtnClick:(UIButton*)sender;
#pragma mark-个人中心
- (void)addPersonalButton;

@end
