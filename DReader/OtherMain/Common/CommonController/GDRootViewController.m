//
//  GDRootViewController.m
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDRootViewController.h"
#import "GDSideViewController.h"
@interface GDRootViewController ()

@end

@implementation GDRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self setNavigationAlpha0];
}
- (void)navigation_CenterView {
    NSArray *viewCArr = [self.navigationController viewControllers];
    long previousVCIndex = [viewCArr indexOfObject:self] - 1;
    UIViewController *previous;
    if (previousVCIndex >= 0) {
        previous = [viewCArr objectAtIndex:previousVCIndex];
        previous.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:self action:nil];
    }
    
    
}
#pragma Mark - 导航透明
- (void)setNavigationAlpha0 {
//    [self navigation_CenterView];
    [self.navigationController.navigationBar setBackgroundImage:GDImage(@"navigation_alpha") forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    _navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, 64)];
    _navBgView.backgroundColor = mainColor;
    [self.view addSubview:_navBgView];
#if 0
    [self.navigationController.navigationBar setBackgroundImage:[GDOperationManager createImageWithColor:mainColor] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = item;
#endif
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = item;
//    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
}

#pragma Mark - 左按钮
- (void)setBackNavigationButton {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 20, 50, 44);
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [backButton setImage:[UIImage imageNamed:@"navigation_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(navigationBackBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.navBgView addSubview:backButton];

}
- (void)navigationBackBtnClick:(UIButton*)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setLeftButtonWithImage:(NSString*)image {
    if (image == nil) {
        image = @"navigation_back";
    }
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0, 0, 27, 27);
    [leftButton setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(rootLeftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
//    self.navigationItem.leftBarButtonItem = leftItem;
    [self.navBgView addSubview:leftButton];
}
- (void)rootLeftBtnClick:(UIButton*)sender {
    
}

#pragma Mark - 右1按钮
- (void)setRightButtonWithImage:(NSString*)image {
    if (image == nil) {
        image = @"";
    }
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 27, 27);
    [rightButton setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rootRightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
//    self.navigationItem.rightBarButtonItem = rightItem;
    [self.navBgView addSubview:rightButton];

}
- (void)rootRightBtnClick:(UIButton*)sender {
    
}

- (void)pushViewController:(UIViewController *)viewController {
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark-个人中心
- (void)addPersonalButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(10, 25, 34, 34);
    button.backgroundColor = [UIColor yellowColor];
    button.layer.cornerRadius = 17;
    button.clipsToBounds = YES;
    [button addTarget:self action:@selector(personalBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.navBgView addSubview:button];
    
}
- (void)personalBtnClick {
    [AppSideVC showLeftViewController:YES];
}

#pragma mark - 禁止同时点击
- (void)setExclusiveTouchForButtons:(UIView *)myView {
    for (UIView *v in [myView subviews]) {
        if ([v isKindOfClass:[UIButton class]]) {
            [((UIButton*)v) setExclusiveTouch:YES];
        }else if ([v isKindOfClass:[UIView class]]) {
            [self setExclusiveTouchForButtons:v];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    AppSideVC.needSwipeShowMenu = NO;
    [self setExclusiveTouchForButtons:self.view];//禁止同时点击按钮
    
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
