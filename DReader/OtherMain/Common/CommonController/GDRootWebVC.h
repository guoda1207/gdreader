//
//  GDRootWebVC.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDRootViewController.h"
#import <WebKit/WebKit.h>


@interface GDRootWebVC : GDRootViewController<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic,strong) WKWebView *gdwebView;

@property (strong, nonatomic) UIProgressView *progressView;


@property (nonatomic, strong) NSString *urlString;

@end
