//
//  GDRootWebVC.m
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDRootWebVC.h"
#import "GDHeader.h"

@interface GDRootWebVC ()

@end

@implementation GDRootWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _gdwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, SCREENWIDTH, SCREENHEIGHT-64)];
    _gdwebView.UIDelegate = self;
    _gdwebView.navigationDelegate = self;
    [self.view addSubview:_gdwebView];
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 1)];
    [self.view addSubview:_progressView];
 
    [_gdwebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];

    __weak typeof(self) weakSelf = self;

    _gdwebView.scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf.gdwebView reload];
    }];
    
    if ([self.urlString hasPrefix:@"https://"] || [self.urlString hasPrefix:@"www."]||[self.urlString hasPrefix:@"http://"]){
        NSURL *url = [NSURL URLWithString:[self.urlString encodeToPercentEscapeString]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setValue:[NSString stringWithFormat:@"%@/ios %@",TARGET_NAME_EN,[GDOperationManager deviceString]] forHTTPHeaderField:@"User-Agent"];
        [request setValue:[GDOperationManager getCurrentVersion] forHTTPHeaderField:@"X-App-Version"];
        [request setValue:@"apple" forHTTPHeaderField:@"X-App-Channel"];
        [request setValue:@"1" forHTTPHeaderField:@"X-APP-FC"];
        [request setValue:@"1" forHTTPHeaderField:@"X-APP-FC2"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@",@"token"] forHTTPHeaderField:@"Authorization"];
        
        [_gdwebView loadRequest:request];
    }else {
        GDLog(@"url 有问题");
    }
    
    
}
//WKNavigationDelegate
/**
 *  页面开始加载时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
    //    NSLog(@"%s", __FUNCTION__);
    
}
/**
 *  当内容开始返回时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
    //    NSLog(@"%s", __FUNCTION__);
}
/**
 *  页面加载完成之后调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [webView evaluateJavaScript:@"document.title" completionHandler:^(id _Nullable obj, NSError * _Nullable error) {
        self.title = [NSString stringWithFormat:@"%@",obj];
    }];
    
    NSLog(@"加载成功");
    [_gdwebView.scrollView.mj_header endRefreshing];
    [_progressView setProgress:0.0 animated:false];
    
}

/**
 *  加载失败时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 *  @param error      错误
 */
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
    [_gdwebView.scrollView.mj_header endRefreshing];
    NSLog(@"加载失败");

}

/**
 *  接收到服务器跳转请求之后调用
 *
 *  @param webView      实现该代理的webview
 *  @param navigation   当前navigation
 */
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    
    //    NSLog(@"%s", __FUNCTION__);
}
////发送请求前决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{

    NSURL *url = navigationAction.request.URL;
    if (url != nil) {
        NSString *urlString = url.absoluteString;
        if (urlString) {
            if ([urlString isEqualToString:self.urlString]){
                decisionHandler(WKNavigationActionPolicyAllow);
                return;
            }else if ([urlString isEqualToString:BASEHTTPURL]){
                decisionHandler(WKNavigationActionPolicyCancel);
            }else if ([urlString hasPrefix:JSBRIDGEHEADER]){
                
                //优惠券
                if ([urlString containsString:[NSString stringWithFormat:@"%@MethodCall/interactProxy#",JSBRIDGEHEADER]]) {
                    NSString *dicStr = [urlString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@MethodCall/interactProxy#",JSBRIDGEHEADER] withString:@""];
                    NSDictionary *dic = dicStr.jsonToDictionary;
                    if ([dic.allKeys containsObject:@"action"]&&[dic.allKeys containsObject:@"params"]&&[dic.allKeys containsObject:@"success"]&&[dic.allKeys containsObject:@"failed"]) {
                        
                        [self weixinAipayRequestAction:dic[@"action"] params:dic[@"params"] completion:^(BOOL isSuc, NSString *jsonString) {
                            if (isSuc) {
                                NSString *sucJson = [NSString stringWithFormat:@"%@(%@)",dic[@"success"],jsonString];
                                [webView evaluateJavaScript:sucJson completionHandler:^(id _Nullable obj, NSError * _Nullable error) {
                                }];
                            }else {
                                NSString *failedJson = [NSString stringWithFormat:@"%@(%@)",dic[@"failed"],jsonString];
                                [webView evaluateJavaScript:failedJson completionHandler:^(id _Nullable obj, NSError * _Nullable error) {
                                }];
                            }
                        }];
                        
                    }
                    decisionHandler(WKNavigationActionPolicyCancel);
                }
                else if ([urlString containsString:[NSString stringWithFormat:@"%@MethodCall/showMessage#",JSBRIDGEHEADER]]){
                    NSString *dicStr = [urlString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@MethodCall/showMessage#",JSBRIDGEHEADER] withString:@""];
                    NSDictionary *dic = dicStr.jsonToDictionary;
                    if ([dic.allKeys containsObject:@"message"]) {
                        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:dic[@"message"] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
                        [alertVC addAction:action];
                        [self presentViewController:alertVC animated:YES completion:nil];
                    }
                    decisionHandler(WKNavigationActionPolicyCancel);
                }
                else if ([urlString containsString:[NSString stringWithFormat:@"%@Navigator/reader#",JSBRIDGEHEADER]]){
                    
                    NSString *dicStr = [urlString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@Navigator/reader#",JSBRIDGEHEADER] withString:@""];
                    NSDictionary *dic = dicStr.jsonToDictionary;
                    if ([dic.allKeys containsObject:@"book_id"]) {
                        //去阅读器
                        GDLog(@"去阅读器");
                        decisionHandler(WKNavigationActionPolicyCancel);
                        
                    }else {
                        decisionHandler(WKNavigationActionPolicyAllow);
                    }
                }
                else if ([urlString containsString:[NSString stringWithFormat:@"%@MethodCall/pay#",JSBRIDGEHEADER]]){
                    NSString *dicStr = [urlString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@MethodCall/pay#",JSBRIDGEHEADER] withString:@""];
                    NSDictionary *dic = dicStr.jsonToDictionary;
                    if ([dic.allKeys containsObject:@"order_fee"]&&[dic.allKeys containsObject:@"channel_id"]&&[dic.allKeys containsObject:@"type"]&&[dic.allKeys containsObject:@"user_coupon_id"]) {
                       
                        GDLog(@"去支付");
                        decisionHandler(WKNavigationActionPolicyCancel);

                    }else{
                        decisionHandler(WKNavigationActionPolicyAllow);

                    }
                }
                
                
                
            }else {
                
                [self webToWebVC:urlString];
                
                decisionHandler(WKNavigationActionPolicyCancel);

            }
            
        }else{
            decisionHandler(WKNavigationActionPolicyAllow);
        }
    }else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
    
}


//进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqual: @"estimatedProgress"] && object == _gdwebView) {
        [self.progressView setAlpha:1.0f];
        [self.progressView setProgress:_gdwebView.estimatedProgress animated:YES];
        if(_gdwebView.estimatedProgress >= 1.0f)
        {
            [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.progressView setProgress:0.0f animated:NO];
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
//切换h5界面
- (void)webToWebVC:(NSString*)url {
    if ([url containsString:@"about:blank"]) {
        return;
    }
    GDLog(@"%@",url);
    NSString *title = @"";
    if ([url containsString:@"search"]) {
        title = @"分类";
    }else if ([url containsString:@"top"]){
        title = @"榜单";
    }else if ([url containsString:@"free"]){
        title = @"免费";
    }else if ([url containsString:@"pay"]){
        
        GDLog(@"去支付界面");
        return;
    }else if ([url containsString:@"book"]) {
        NSArray *array = [url componentsSeparatedByString:@"/book/"];
        if (array.count > 1) {
            NSString *bookID = [array[1] stringByReplacingOccurrencesOfString:@".html" withString:@""];
            if (bookID.integerValue > 0){
                GDLog(@"详情页");
            }
        }
        
    }else{
        title = @"详情";
    }
    GDRootWebVC *webVC = [[GDRootWebVC alloc] init];
    webVC.urlString = url;
    [self pushViewController:webVC];
    
    
}

#pragma mark - 获取优惠券
- (void)weixinAipayRequestAction:(NSString*)action params:(NSString*)params completion:(nullable void (^)(BOOL isSuc,NSString *jsonString))completion{
    
    [[GD_DownloadCenter manager] postRequestWithURL:@"" parameters:@{@"":@""} callBlock:^(id responseObject) {
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        completion(YES,string);
        
    } callError:^(id Error) {
        NSError *error = (NSError*)Error;
        NSString *code = [NSString stringWithFormat:@"%ld",(long)error.code];
        NSString *msg = [NSString stringWithFormat:@"%@",error.description];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:code forKey:@"code"];
        [dic setObject:msg forKey:@"message"];
        NSString *json = dic.dictionaryToJson;
        completion(NO,json);
    }];
    
}



- (void)dealloc {
    if (_gdwebView) {
        [_gdwebView removeObserver:self forKeyPath:@"estimatedProgress"];
        [_gdwebView setNavigationDelegate:nil];
        [_gdwebView setUIDelegate:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
