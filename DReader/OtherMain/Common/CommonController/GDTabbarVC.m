//
//  GDTabbarVC.m
//  DReader
//
//  Created by moqing on 2017/9/1.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDTabbarVC.h"
#import "TopicViewController.h"
#import "ShelfViewController.h"
#import "BookStoreViewController.h"
#import "GDListViewController.h"
#import "GDHeader.h"
@interface GDTabbarVC ()

@end

@implementation GDTabbarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TopicViewController *topic = [[TopicViewController alloc] init];
    [self addChildVc:topic Title:@"专题" image:[UIImage imageNamed:@"tab_found"] selectedImage:[UIImage imageNamed:@"tab_found_sel"]];
    BookStoreViewController *bookstore = [[BookStoreViewController alloc] init];
    [self addChildVc:bookstore Title:@"书城" image:[UIImage imageNamed:@"tab_home"] selectedImage:[UIImage imageNamed:@"tab_home_sel"]];
    ShelfViewController *shelf = [[ShelfViewController alloc] init];
    [self addChildVc:shelf Title:@"书架" image:[UIImage imageNamed:@"tab_shelf"] selectedImage:[UIImage imageNamed:@"tab_shelf_sel"]];
    GDListViewController *gdlist = [[GDListViewController alloc] init];
    [self addChildVc:gdlist Title:@"我的" image:GDImage(@"tab_mine") selectedImage:GDImage(@"tab_mine_sel")];
    self.tabBar.tintColor = mainColor;
    self.selectedIndex = 1;

}
#pragma mark - 添加一个子控制器
- (void)addChildVc:(UIViewController *)Vc Title:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage
{
    // 不调用此方法, 不会创建View
    
    // 设置子控制器的图片
    Vc.tabBarItem.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];//始终绘制图片原始状态，不适用tint
    /**  取消自动渲染 ***/
    Vc.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    Vc.tabBarItem.selectedImage = selectedImage;
    
    // 设置标题
//    Vc.title = title;
    
    Vc.tabBarItem.title = title;
    
    [Vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : mainColor, NSFontAttributeName:[UIFont systemFontOfSize:12]} forState:UIControlStateSelected];
    [Vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor], NSFontAttributeName:[UIFont systemFontOfSize:12]} forState:UIControlStateNormal];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:Vc];

    [self addChildViewController:nav];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
