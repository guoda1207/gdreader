//
//  CustomDefineCenter.h
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#ifndef CustomDefineCenter_h
#define CustomDefineCenter_h

typedef NS_ENUM(NSUInteger,Append_Type) {
    Append_Type_Normal = 0,
    Append_Type_OS,
    Append_Type_MCU
};


/**3
 *  屏幕的大小
 */
#define SCREENWIDTH [UIScreen mainScreen].bounds.size.width

#define SCREENHEIGHT [UIScreen mainScreen].bounds.size.height


/**
 *  设置图片
 *
 *  @param X 图片名称
 *
 *  @return UIImage类型的
 */
#define GDImage(X) [UIImage imageNamed:X]

#define GDLocalImage(x) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:x ofType:@"png"]]


/*
 *  颜色
 */
#define GDColor(rgbValue,alp) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alp]

//#define RGBColor(r,g,b) [UIColor colorWithRed:(float)(r)/255.0 green:(float)(g)/255.0 blue:(float)(b)/255.0 alpha:1]
#define RGBColor(r,g,b) RGBAColor(r,g,b,1.0)

#define RGBAColor(r,g,b,alp) [UIColor colorWithRed:(float)(r)/255.0 green:(float)(g)/255.0 blue:(float)(b)/255.0 alpha:alp]

/*
 *  weakSelf
 */
#define WeakSelf(weakSelf)  __weak typeof(self)weakSelf = self;


/*
 *  系统字体大小
 */
#define GD_Font(size) [UIFont systemFontOfSize:size]

#define GD_boldFont(size) [UIFont boldSystemFontOfSize:size]

#define GD_italicFont(size) [UIFont italicSystemFontOfSize:size]

/**
 打印

 @param s
 @param ...
 @return
 */
#ifdef DEBUG
#define GDLog( s, ... ) NSLog( @"^_^[%@:(%d)] %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] );
#else
#define GDLog( s, ... )
#endif


/*
 *  简写
 */
#define AppDelegate [[UIApplication sharedApplication] delegate]

#define AppSideVC ((GDAppDelegate*)[[UIApplication sharedApplication] delegate]).sideViewController

//sql path
#define Local_Home_SQLInfo_Path ([NSString stringWithFormat:@"%@/Library/MQDB",NSHomeDirectory()])



#endif /* CustomDefineCenter_h */
