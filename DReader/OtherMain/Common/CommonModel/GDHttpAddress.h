//
//  GDHttpAddress.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#ifndef GDHttpAddress_h
#define GDHttpAddress_h

//专题
#define foundHttpURL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"topic"];
//QQ登录
#define QQLoginHttpUTL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"other_login/from/qq"];

////消费记录
#define coastHttpURL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"member/trade/cost"];
//充值记录
#define orderHttpURL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"member/trade/order"];

//充值页面
#define payHttpURL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"pay/index/v3"]{

//帮助页面
#define helpHttpURL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"main/help"]{

//反馈
#define feedBackHttpURL [NSString stringWithFormat:@"%@%@",BASEHTTPURL,@"main/feedback"]{


//app

#define loginPath @"login"
#define wxLoginPath @"login_wechat"
#define wbLoginPath @"login_weibo"
#define qqLoginPath @"login_qq"

#define outSideLoginPath @"login_outside"
#define bindMobilePath @"bind_phone"
#define registerPath @"register"

#define uploadUserInfoPath @"update_user_info"

#define authorizationPath @"authorization"
#define phoneVertifyCodePath @"phone_verify_code"

#define userCoinPath @"surplus"
#define userSign @"signin"
#define userChangePswPath @"reset_pass"
#define userReward @"reward"

#define bookShelfPushPath @"bookshelf_push"
#define bookshelf_sync @"bookshelf_sync"
#define book_recommends @"book_recommends"
#define bookshelf_add @"bookshelf_add"

#define chapterSubscribe @"subscribe"

#define bookListPath @"book_index_incr"
#define chapterContentPath @"chapter"
#define vipChapterContentPath @"book_chapters"
#define chapterContentByIdsPath @"book_chapters_by_ids"

#define bookInfoPath @"book"
#define otherBooks @"author_book"
#define infoRecommends @"get_tjs"
#define infoComments @"comment_list"
#define infoToVote @"vote"

#define applePayTypePath @"apple_review_judge"
#define autoRegisterPath @"auto_register"

#define appleOrderPath @"order"
#define wxOrderPath @@"wechat_order"
#define alipayOrderPath @"get_alipay_config"
#define alipayOrderPath_web @@"get_alipay_config_wap"
#define wxOrderPath_web @"wechat_order_wap"
#define appleVerifyPath @"apple_pay"

#define sharedCallBackpath @"share_callback"
#define userMobileCheckPath @"user_check"

#define user_order @"order_list"
#define user_cost @"cost_gather"
#define user_costInfo @"cost_list"

#define hot_search @"hot_search"
#define search_book @"search_book"
#define comment @"comment"
#define comment_vote @"comment_vote"

#define getAllFreeChapterContentsPath @"free_chapters"
#define getAllSubscribeChapterPath @"get_cost_chapters"

#define getIpaynowPath @"get_ipaynow_config"

#define qqWebLoginPath @"login_qq_web"
//书城banner
#define bookstore_Banner @"main_banners"
//获取书城导航
#define bookstore_nav @"main_navigation"
//获取推荐位列表
#define bookstore_recommends @"main_recommends"
//根据书籍获取不同的推荐书籍
//#define bookstore_bidrec @"book/{book id}/recommend"
#define userCheckExists @"user_check"
//MARK:更新用户信息 user  签到
#define update_userInfo @"user"
//获取可用优惠券
#define vaildCouponPath @"valid_coupons"
//领取优惠券
#define takeCouponPath @"take_coupon"
//我的优惠券
#define userCouponPath @"my_coupons"
//请求idfa记录
#define userIDFAPath @"upload_idfa"
//h5 请求通用接口
#define h5communal_action @"communal_action"
//打赏记录
#define user_RewardLog @"reward_list"
//书包购买记录
#define user_packageLog @"package_list"
//单本订阅记录
#define user_wholesubscribelist @"whole_subscribe_list"
//查询书籍限免信息
#define query_freelimit @"query_freelimit"


#endif /* GDHttpAddress_h */
