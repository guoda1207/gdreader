//
//  LocalSavePath.h
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalSavePath : NSObject
+ (instancetype)shareInstance;

- (void) localSavePath_createPath;

@end
