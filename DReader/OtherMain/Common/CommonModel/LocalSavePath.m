//
//  LocalSavePath.m
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "LocalSavePath.h"
#import "GDHeader.h"
@implementation LocalSavePath
+ (instancetype)shareInstance {
    
    static LocalSavePath *manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void) localSavePath_createPath {
    NSString *homePath = Local_Home_SQLInfo_Path;
    [self createPathWithPath:homePath];
}


- (void)createPathWithPath:(NSString *)path {

    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL yes;
    if (![manager fileExistsAtPath:path isDirectory:&yes]) {
        BOOL b=[manager createDirectoryAtPath:path withIntermediateDirectories:yes attributes:nil error:nil];
        GDLog(@"b=%d",b);
    }
    
}

@end
