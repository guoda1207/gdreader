//
//  TargetDefine.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#ifndef TargetDefine_h
#define TargetDefine_h


#ifndef DReader

#define mainColor [UIColor colorWithRed:0/255.0 green:151.0/255 blue:167.0/255 alpha:1]

#define CopyRightName @"魔情"

#define Target @"MQ"

#define CoinName @"魔币"

#define CoinName_Preium @"魔豆"

#define JSBRIDGEHEADER @"mqappjsbridge://"

#define TARGET_NAME @"魔情阅读"

#define TARGET_NAME_EN @"MoqingApp"

#define targetImageName @"mq"




#endif



#endif /* TargetDefine_h */
