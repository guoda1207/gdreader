//
//  GDBaseSQL.m
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDBaseSQL.h"

@implementation GDBaseSQL

+ (instancetype)shareInstance {
    
    static GDBaseSQL *manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[self alloc] init];
    });
    return manager;
}


@end
