//
//  GDFMDB.h
//  DReader
//
//  Created by moqing on 2017/9/8.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GDFMDB : NSObject

+ (instancetype)shareInstance;

- (BOOL)jq_createTable:(NSString *)tableName dicOrModel:(id)parameters excludeName:(NSArray *)nameArr;

@end
