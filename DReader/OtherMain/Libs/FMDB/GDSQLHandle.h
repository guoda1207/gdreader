//
//  GDSQLHandle.h
//  DReader
//
//  Created by moqing on 2017/9/27.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>
static NSString *intType     = @"i"; // int_32t,int

static NSString *longlongType = @"q"; // long,或者longlong

static NSString *floatType   = @"f"; // float

static NSString *doubleType  = @"d"; // double

static NSString *boolType    = @"B"; // bool

static NSString *imageType   = @"UIImage"; // UIImage 类型

static NSString *stringType  = @"NSString"; // NSString 类型

static NSString *numberType  = @"NSNumber"; // NSNumber 类型

@interface GDSQLHandle : NSObject

+ (instancetype)shareInstance;

//- (NSString *)createTable:(NSString*)tableName dictionary:(NSDictionary*)dic excludeName:(NSArray*)nameArr;
- (NSString *)createTable:(NSString*)tableName model:(id)cls excludeName:(NSArray*)nameArr;


@end
