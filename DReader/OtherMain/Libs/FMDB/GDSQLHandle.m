//
//  GDSQLHandle.m
//  DReader
//
//  Created by moqing on 2017/9/27.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDSQLHandle.h"
#import "GDHeader.h"

@interface GDSQLHandle ()

@property (nonatomic, strong)NSString *dbPath;
@property (nonatomic, strong)FMDatabaseQueue *dbQueue;
@property (nonatomic, strong)FMDatabase *db;

@end
@implementation GDSQLHandle

+ (instancetype)shareInstance {
    
    static GDSQLHandle *manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[self alloc] init];
    });
    return manager;
}
- (NSString *)createTable:(NSString *)tableName Ivars:(NSArray*)ivars Keys:(NSArray*)keys {
    NSMutableString *fieldStr = [[NSMutableString alloc] initWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (pkid  INTEGER PRIMARY KEY autoincrement,", tableName];
    for (int count = 0; count < keys.count; count++) {
        NSString *key = keys[count];
        if ([key isEqualToString:@"pkid"]) {
            continue;
        }
        if (count == keys.count - 1) {
            [fieldStr appendFormat:@" %@ %@)",key,ivars[count]];
            break;
        }
        [fieldStr appendFormat:@" %@ %@,",key,ivars[count]];
    }
    
    
    
//    int keyCount = 0;
//    for (NSString *key in dic) {
//        keyCount ++;
//        if ([key isEqualToString:@"pkid"]) {
//            continue;
//        }
//        if (keyCount == dic.count) {
//            [fieldStr appendFormat:@" %@ %@)",key, dic[key]];
//            break;
//        }
//        [fieldStr appendFormat:@" %@ %@,",key,dic[key]];
//    }
    return fieldStr;

}
//- (NSString *)createTable:(NSString*)tableName dictionary:(NSDictionary*)dic
//{
//    NSMutableString *fieldStr = [[NSMutableString alloc] initWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (pkid  INTEGER PRIMARY KEY autoincrement,", tableName];
//    int keyCount = 0;
//    for (NSString *key in dic) {
//        keyCount ++;
//        if ((nameArr && [nameArr containsObject:key]) || [key isEqualToString:@"pkid"]) {
//            continue;
//        }
//        if (keyCount == dic.count) {
//            [fieldStr appendFormat:@" %@ %@)",key, dic[key]];
//            break;
//        }
//        [fieldStr appendFormat:@" %@ %@,",key,dic[key]];
//    }
//    return [self createTable:tableName Ivars:@[@"1",@"2"] dictionary:dic];

//}
- (NSString *)createTable:(NSString*)tableName model:(id)cls excludeName:(NSArray*)nameArr {
    NSDictionary *dic = nil;
    if ([cls isKindOfClass:[NSDictionary class]]) {
        dic = cls;
    }else {
        dic = [cls tools_properties_class:cls];
    }
    long count = dic.allKeys.count;
    NSMutableArray *mutableArray = [NSMutableArray array];
    NSMutableArray *typeArray = [NSMutableArray array];
    Ivar *ivars = class_copyIvarList([cls class], nil);
    for (int i=0; i<count; i++) {
        Ivar ivar = ivars[i];
        //根据ivar获得成员变量的名称
        const char *name = ivar_getName(ivar);
        NSLog(@"%s",name);
        NSString *key = [NSString stringWithUTF8String:name];
        //放入数组
        NSString *keyString = [key stringByReplacingOccurrencesOfString:@"_" withString:@""];
        [mutableArray addObject:keyString];
        //获取变量类型c字符串
        const char *cType = ivar_getTypeEncoding(ivar);
        //C的字符串转OC的字符串
        NSString *Type = [NSString stringWithUTF8String:cType];
        //基本类型数组库类型转化
        
        NSLog(@"======%@",Type);
        
        NSString *replaceString = [self replaceStringWithCString:Type];
        [typeArray addObject:replaceString];
    }
    NSLog(@"%@",typeArray);
    return [self createTable:tableName Ivars:typeArray Keys:mutableArray];
}
- (NSString*)replaceStringWithCString:(NSString*)cString {
    if (![cString isEqualToString:@""]) {
        
        if ([cString isEqualToString:@"i"]) {
            
            return @"int";
            
        }else if([cString isEqualToString:@"q"]){
            
            return @"double";
            
        }else if([cString isEqualToString:@"f"]){
            
            return @"float";
            
        }else if([cString isEqualToString:@"d"]){
            
            return @"double";
            
        }else if([cString isEqualToString:@"B"]){
            
            return @"int";
            
        }else if([cString containsString:@"NSString"]){
            
            return @"text";
            
        }else if([cString containsString:@"NSNumber"]){
            
            return @"long";
            
        }
        NSAssert(1, @"handleSqliteTable类中 model的属性状态不对导致数据库状态不对，请核对后再拨");
        return @"未知";
    }else{
        return nil;
    }
}

@end
