//
//  GDUserSQL.m
//  DReader
//
//  Created by moqing on 2017/8/29.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDUserSQL.h"

@implementation GDUserSQL

+ (instancetype)shareInstance {

    static GDUserSQL *manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[self alloc] init];
    });
    return manager;
}
- (void)SQL_InitWith_UserInfo {

    NSString *path = [NSString stringWithFormat:@"%@.db",Local_Home_SQLInfo_Path];
    _queue = [[FMDatabaseQueue alloc] initWithPath:path];
    NSString *sql = @"create table if not exists userInfo(user_id varchar(100),sign_in varchar(32),user_avatar varchar(100),user_coin varchar(32),user_info_complete varchar(32),user_level varchar(32),user_mobile varchar(32),user_mobile_verify varchar(32),user_nick varchar(100),user_point varchar(32),user_premium varchar(32),user_ticket varchar(32),user_vip_expiry varchar(32),user_vip_level varchar(32),access_token varchar(500),refresh_token varchar(500))";
    [_queue inDatabase:^(FMDatabase * _Nonnull db) {
        if (!db.open) {
            GDLog(@"user info");
            return;
        }
        [db executeUpdate:sql];
    }];
}
- (void)insert_userInfoWithUserid:(NSString*)user_id sign_in:(NSString*)sign_in user_avatar:(NSString*)user_avatar user_coin:(NSString*)user_coin user_info_complete:(NSString*)user_info_complete user_level:(NSString*)user_level user_mobile:(NSString*)user_mobile user_mobile_verify:(NSString*)user_mobile_verify user_nick:(NSString*)user_nick user_point:(NSString*)user_point user_premium:(NSString*)user_premium user_ticket:(NSString*)user_ticket user_vip_expiry:(NSString*)user_vip_expiry user_vip_level:(NSString*)user_vip_level access_token:(NSString*)access_token refresh_token:(NSString*)refresh_token {
    [_queue inDatabase:^(FMDatabase * _Nonnull db) {
        NSString *sql = @"insert into userInfo(user_id,sign_in,user_avatar,user_coin ,user_info_complete ,user_level ,user_mobile ,user_mobile_verify ,user_nick ,user_point ,user_premium ,user_ticket ,user_vip_expiry ,user_vip_level ,access_token ,refresh_token) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        BOOL b = [db executeUpdate:sql,user_id,sign_in,user_avatar,user_coin,user_info_complete,user_level,user_mobile,user_mobile_verify,user_nick,user_point,user_premium,user_ticket,user_vip_expiry,user_vip_level,access_token,refresh_token];
        GDLog(@"%d",b);
    }];

}
@end
