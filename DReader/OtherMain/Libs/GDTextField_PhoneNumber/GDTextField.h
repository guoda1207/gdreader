//
//  GDTextField.h
//  XUIPhone
//
//  Created by xiaoyu on 16/2/26.
//  Copyright © 2016年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^GetPhoneNumber)(NSString *number);

@interface GDTextField : UITextField

@property (nonatomic, copy) GetPhoneNumber getNumber;

- (void)getNumber:(GetPhoneNumber)block;

@end
