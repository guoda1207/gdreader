//
//  YanzhengmaTextField.h
//  XUIPhone
//
//  Created by xiaoyu on 16/2/26.
//  Copyright © 2016年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^Yanzhengma)(NSString *number);

@interface YanzhengmaTextField : UITextField

@property (nonatomic, copy) Yanzhengma getNumber;

- (void)getNumber:(Yanzhengma)block;


@end
