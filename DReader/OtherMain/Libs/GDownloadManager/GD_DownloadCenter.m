//
//  GD_DownloadCenter.m
//  GDDownloadManager
//
//  Created by xiaoyu on 15/11/20.
//  Copyright © 2015年 guoda. All rights reserved.
//

#import "GD_DownloadCenter.h"
#import "GDHeader.h"
@implementation GD_DownloadCenter
+ (GD_DownloadCenter*)manager {
    static GD_DownloadCenter *gdmanager = nil;
    static dispatch_once_t hello;
    dispatch_once(&hello, ^{
        gdmanager = [[self alloc] init];
        gdmanager.requestManager = [AFHTTPSessionManager manager];
        gdmanager.requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];

    });
    return gdmanager;
}
- (void)postRequestWithURL:(NSString *)port parameters:(NSDictionary *)parameters callBlock:(GD_ResultBlockSuccess)callBlock callError:(GD_ResultBlockError)callerror
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",baseURL,port];
    self.requestManager.requestSerializer.timeoutInterval = 60;
    [self.requestManager POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        callBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callerror(error);
    }];
    
}
- (void)getRequestWithURL:(NSString *)port parameters:(NSDictionary *)parameters callBlock:(GD_ResultBlockError)callBlock callError:(GD_ResultBlockError)callerror{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",baseURL,port];
    self.requestManager.requestSerializer.timeoutInterval = 60;
    [self.requestManager GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        callBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callerror(error);
    }];
    
}

@end
