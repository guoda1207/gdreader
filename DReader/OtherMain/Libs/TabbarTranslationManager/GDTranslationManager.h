//
//  GDTranslationManager.h
//  TabbarChangeAnimations
//
//  Created by X-Designer on 17/2/20.
//  Copyright © 2017年 Guoda. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger,TabbarTransType) {
    TabbarLeft,
    TabbarRight
};
@interface GDTranslationManager : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) TabbarTransType tabType;

@end
