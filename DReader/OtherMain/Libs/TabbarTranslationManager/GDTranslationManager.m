//
//  GDTranslationManager.m
//  TabbarChangeAnimations
//
//  Created by X-Designer on 17/2/20.
//  Copyright © 2017年 Guoda. All rights reserved.
//

#import "GDTranslationManager.h"

@implementation GDTranslationManager

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return 0.50;
}
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    UIViewController *tovc = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromvc = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *containerView = [transitionContext containerView];
    if (!tovc||!fromvc||!containerView) return;
    
    tovc.view.transform = CGAffineTransformIdentity;
    fromvc.view.transform = CGAffineTransformIdentity;
    
    CGFloat translation = containerView.frame.size.width;
    
    switch (self.tabType) {
        case TabbarLeft:
            translation = translation;
            break;
        case TabbarRight:
            translation = -translation;
        default:
            break;
    }
    
    [containerView addSubview:tovc.view];
    tovc.view.transform = CGAffineTransformMakeTranslation(-translation, 0);
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromvc.view.transform = CGAffineTransformMakeTranslation(translation, 0);
        tovc.view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        fromvc.view.transform = CGAffineTransformIdentity;
        tovc.view.transform = CGAffineTransformIdentity;
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
    
    
}

@end
