//
//  GDButton.h
//  GDButton
//
//  Created by xiaoyu on 15/11/19.
//  Copyright © 2015年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GDButton : UIButton

@property (nonatomic, copy) void (^action)(UIButton *button);


@end
