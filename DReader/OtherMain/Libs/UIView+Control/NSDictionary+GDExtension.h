//
//  NSDictionary+GDExtension.h
//  DReader
//
//  Created by moqing on 2017/9/26.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (GDExtension)


/**
 字典转json

 @return json
 */
- (NSString *)dictionaryToJson;


@end
