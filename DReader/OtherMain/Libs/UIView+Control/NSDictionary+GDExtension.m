//
//  NSDictionary+GDExtension.m
//  DReader
//
//  Created by moqing on 2017/9/26.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "NSDictionary+GDExtension.h"
#import "GDHeader.h"
@implementation NSDictionary (GDExtension)

#pragma mark - 字典转json
- (NSString *)dictionaryToJson {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = @"";
    if (!jsonData){
        GDLog(@"convertToJson Error :%@",error);
    }else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];//去除掉首尾的空白字符和换行字符
    [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return jsonString;
}


@end
