//
//  NSObject+Extension.h
//  DReader
//
//  Created by moqing on 2017/9/27.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Extension)

/**
 字典转实例化model

 @param dictionary 需要转化的字典
 @return 字典要转成的model
 */
- (id)tools_serializationWithDictionary:(NSDictionary*)dictionary;


/**
 把model类转化成字典，方便打印看值
 
 @param superClass model类
 
 @return key-value
 */
- (NSDictionary *)tools_properties_class:(id)superClass;
@end
