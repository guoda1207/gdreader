//
//  NSObject+Extension.m
//  DReader
//
//  Created by moqing on 2017/9/27.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "NSObject+Extension.h"
#import "GDHeader.h"
#import <objc/runtime.h>
@implementation NSObject (Extension)



- (id)tools_serializationWithDictionary:(NSDictionary*)dictionary
{
    return [self convert:dictionary];
}

- (id)convert:(NSDictionary*)dataSource
{
    NSArray *objkeys = [self propertyKeys];
    for (NSString *key in [dataSource allKeys]) {
        if ([objkeys containsObject:key]) {
            id propertyValue = [dataSource valueForKey:key];
            if (![propertyValue isKindOfClass:[NSNull class]]
                && propertyValue != nil) {
                
                [self setValue:propertyValue
                        forKey:key];
            }
        }
    }
    return self;
}

- (NSArray*)propertyKeys
{
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    NSMutableArray *propertys = [NSMutableArray arrayWithCapacity:outCount];
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        [propertys addObject:propertyName];
    }
    free(properties);
    return propertys;
}


/**
 把model类转化成字典，方便打印看值
 
 @param superClass model
 
 @return key-value
 */

- (NSDictionary *)tools_properties_class:(id)superClass{
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int propsCount,i;
    objc_property_t *properties = class_copyPropertyList([superClass class], &propsCount);
    for (i=0; i<propsCount; i++) {
        objc_property_t property = properties[i];
        const char* char_f = property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        id propertyValue = [superClass valueForKey:(NSString*)propertyName];
        if (propertyValue==nil) {
            propertyValue = [NSNull null];
        }else{
            propertyValue = [self tools_getObjectInternal:propertyValue];
        }
        [props setValue:propertyValue forKey:propertyName];
        
    }
    free(properties);
    return props;
}
- (id)tools_getObjectInternal:(id)obj
{
    if([obj isKindOfClass:[NSString class]]
       || [obj isKindOfClass:[NSNumber class]]
       || [obj isKindOfClass:[NSNull class]])
    {
        return obj;
    }
    
    if([obj isKindOfClass:[NSArray class]])
    {
        NSArray *objarr = obj;
        NSMutableArray *arr = [NSMutableArray arrayWithCapacity:objarr.count];
        for(int i = 0;i < objarr.count; i++)
        {
            [arr setObject:[self tools_getObjectInternal:[objarr objectAtIndex:i]] atIndexedSubscript:i];
        }
        return arr;
    }
    
    if([obj isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *objdic = obj;
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:[objdic count]];
        for(NSString *key in objdic.allKeys)
        {
            [dic setObject:[self tools_getObjectInternal:[objdic objectForKey:key]] forKey:key];
        }
        return dic;
    }
    return [self tools_properties_class:obj];
}




@end
