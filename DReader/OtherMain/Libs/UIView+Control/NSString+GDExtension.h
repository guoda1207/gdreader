//
//  NSString+GDExtension.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GDExtension)


/**
 url编码

 @return 编码后的url
 */
- (NSString *)encodeToPercentEscapeString;



/**
 json->dic

 @return dictionary
 */
- (NSDictionary *)jsonToDictionary;
@end
