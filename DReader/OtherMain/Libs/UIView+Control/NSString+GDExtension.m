//
//  NSString+GDExtension.m
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "NSString+GDExtension.h"
#import "GDHeader.h"
@implementation NSString (GDExtension)

#pragma mark - url编码
- (NSString *)encodeToPercentEscapeString
{
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"`#%^{}\"[]|\\<> "].invertedSet];
}

- (NSDictionary *)jsonToDictionary {
    if (self == nil) {
        return nil;
    }
    NSData *jsonData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if (err) {
        GDLog(@"( json -> dic )转化失败");
        return nil;
    }
    return dic;
}

@end
