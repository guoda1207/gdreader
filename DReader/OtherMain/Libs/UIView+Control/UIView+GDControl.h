//
//  UIView+GDControl.h
//  GDButton
//
//  Created by xiaoyu on 15/11/19.
//  Copyright © 2015年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GD_HeaderCenter.h"
@interface UIView (GDControl)
//添加标签
-(UILabel *)addLabelWithFrame:(CGRect)frame
                         text:(NSString *)text;

//添加系统按钮
-(UIButton *)addSystemButtonWithFrame:(CGRect)frame
                                title:(NSString *)title
                               action:(void (^)(UIButton *button))action;
//添加图片按钮
-(UIButton *)addImageButtonWithFrame:(CGRect)frame
                               title:(NSString *)title
                               Image:(NSString*)image
                           backgroud:(NSString *)backgroud
                              action:(void (^)(UIButton *button))action;

//添加图片视图
-(UIImageView *)addImageViewWithFrame:(CGRect)frame
                                image:(NSString *)image;
//添加输入框
-(UITextField *)addTextFieldWithFrame:(CGRect)frame;

/**
 添加搜索按钮

 @return 搜索按钮
 */
- (UIButton*)addNavigationSearchButton_Action:(void (^)(UIButton *button))action;




//添加图片按钮
//-(UIButton *)addGreenButtonWithFrame:(CGRect)frame
//                               title:(NSString *)title
//                              action:(void (^)(UIButton *button))action;
//添加图片按钮
//-(UIButton *)addRedButtonWithFrame:(CGRect)frame
//                               title:(NSString *)title
//                              action:(void (^)(UIButton *button))action;
////添加图片按钮
//-(UIButton *)addBlueButtonWithFrame:(CGRect)frame
//                               title:(NSString *)title
//                              action:(void (^)(UIButton *button))action;
/**
 *  默认长图按钮
 *
 *  @param frame
 *  @param title
 *
 *  @return button
 */
//- (UIButton *)XChangeDefaultButtonWithFrame:(CGRect)frame title:(NSString *)title action:(void (^)(UIButton *button))action;
/**
 *  默认图按钮590
 *
 *  @param frame
 *  @param title
 *
 *  @return button
 */
//- (UIButton *)XCDefaultButtonWithFrame:(CGRect)frame title:(NSString *)title action:(void (^)(UIButton *))action;
/**
 *  弹窗按钮360
 *
 *  @param frame
 *  @param title
 *
 *  @return button
 */
//- (UIButton *)XCAlertButtonWithFrame:(CGRect)frame title:(NSString *)title action:(void (^)(UIButton *))action;

@end
