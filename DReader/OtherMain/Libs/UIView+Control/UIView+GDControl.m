//
//  UIView+GDControl.m
//  GDButton
//
//  Created by xiaoyu on 15/11/19.
//  Copyright © 2015年 guoda. All rights reserved.
//

#import "UIView+GDControl.h"
#import "GDButton.h"
#import "GDHeader.h"
//#import "GD_HeaderCenter.h"

@implementation UIView (GDControl)
//添加标签
-(UILabel *)addLabelWithFrame:(CGRect)frame
                         text:(NSString *)text
{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = text;
    [self addSubview:label];
    return label;
}

//添加系统按钮
-(UIButton *)addSystemButtonWithFrame:(CGRect)frame
                                title:(NSString *)title
                               action:(void (^)(UIButton *button))action
{
    GDButton *button = [GDButton buttonWithType:UIButtonTypeSystem];
    button.frame = frame;
    [button setTitle:title forState:UIControlStateNormal];
    button.action = action;
    [self addSubview:button];
    return button;
}
- (UIButton *)addImageButtonWithFrame:(CGRect)frame title:(NSString *)title Image:(NSString *)image backgroud:(NSString *)backgroud action:(void (^)(UIButton *))action {
    
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    if (backgroud != nil) {
        [button setBackgroundImage:[UIImage imageNamed:backgroud] forState:UIControlStateNormal];
    }
    if (image != nil) {
        [button setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    }
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.action = action;
    [self addSubview:button];
    return button;
    
}
//添加图片视图
-(UIImageView *)addImageViewWithFrame:(CGRect)frame
                                image:(NSString *)image
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    if (image != nil) {
        imageView.image = [UIImage imageNamed:image];
//        imageView.image = [UIImage imageWithContentsOfFile:image];
    }
    imageView.userInteractionEnabled = YES;
    [self addSubview:imageView];
    return imageView;
}
//添加输入框
-(UITextField *)addTextFieldWithFrame:(CGRect)frame
{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:textField];
    return textField;
}

#pragma mark - 添加搜索按钮
- (UIView *)addNavigationSearchButton_Action:(void (^)(UIButton *))action {
    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH-30, 44)];
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 8, searchView.w, 44-16);
    button.layer.cornerRadius = 5;
    button.clipsToBounds = YES;
    [button setImage:GDImage(@"navigation_search") forState:UIControlStateNormal];
    [button setTitle:@"书名/作者/关键字" forState:UIControlStateNormal];
    [button setTitleColor:RGBColor(151, 151, 151) forState:UIControlStateNormal];
    button.backgroundColor = RGBColor(236, 251, 255);
    button.titleLabel.font = GD_Font(14);
    button.action = action;
    [searchView addSubview:button];

    return searchView;
}


#if 0
//添加图片按钮
-(UIButton *)addGreenButtonWithFrame:(CGRect)frame
                               title:(NSString *)title
                              action:(void (^)(UIButton *button))action{
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    button.layer.cornerRadius = frame.size.height/2;
    button.clipsToBounds = YES;
    [button setTitleColor:XUIColor(rgbdefaultValue, 1) forState:UIControlStateNormal];
    button.layer.borderWidth = 1;
    button.layer.borderColor = XUIColor(rgbdefaultValue, 1).CGColor;
    [button setTitleColor:XUIColor(0x1a1d21, 1) forState:UIControlStateHighlighted];
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x398b75, 1)] forState:UIControlStateHighlighted];
    button.action = action;
    [self addSubview:button];
    return button;

}
//添加图片按钮
-(UIButton *)addRedButtonWithFrame:(CGRect)frame
                             title:(NSString *)title
                            action:(void (^)(UIButton *button))action{
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
    }
   
    button.layer.cornerRadius = frame.size.height/2;
    button.clipsToBounds = YES;
    [button setTitleColor:XUIColor(0xb74d5e, 1) forState:UIControlStateNormal];
    button.layer.borderWidth = 1;
    button.layer.borderColor = XUIColor(0xb74d5e, 1).CGColor;
    [button setTitleColor:XUIColor(0x1a1d21, 1) forState:UIControlStateHighlighted];
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0xb74d5e, 1)] forState:UIControlStateHighlighted];    button.action = action;
    [self addSubview:button];
    return button;

}
//添加图片按钮
-(UIButton *)addBlueButtonWithFrame:(CGRect)frame
                              title:(NSString *)title
                             action:(void (^)(UIButton *button))action{
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    button.layer.cornerRadius = frame.size.height/2;
    button.clipsToBounds = YES;
    [button setTitleColor:XUIColor(0x089dbc, 1) forState:UIControlStateNormal];
    button.layer.borderWidth = 1;
    button.layer.borderColor = XUIColor(0x089dbc, 1).CGColor;
    [button setTitleColor:XUIColor(0x1a1d21, 1) forState:UIControlStateHighlighted];
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x089dbc, 1)] forState:UIControlStateHighlighted];    button.action = action;
    [self addSubview:button];
    return button;

}
/**
 *  默认可变图按钮686
 *
 *  @param frame
 *  @param title
 *
 *  @return button
 */
- (UIButton *)XChangeDefaultButtonWithFrame:(CGRect)frame title:(NSString *)title action:(void (^)(UIButton *))action{
    
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    button.frame = CGRectMake(frame.origin.x, frame.origin.y, SCREENWIDTH-64, 102/2);
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x3b5286, 1)] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x3b5286, 0.75)] forState:UIControlStateHighlighted];
    button.layer.cornerRadius = 2;
    button.clipsToBounds = YES;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.action = action;
    [self addSubview:button];
    return button;
}
/**
 *  默认图按钮590
 *
 *  @param frame
 *  @param title
 *
 *  @return button
 */
- (UIButton *)XCDefaultButtonWithFrame:(CGRect)frame title:(NSString *)title action:(void (^)(UIButton *))action{
    
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    button.frame = CGRectMake((SCREENWIDTH-590/2)/2, frame.origin.y, 590/2, 102/2);
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x3b5286, 1)] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x3b5286, 0.75)] forState:UIControlStateHighlighted];
    button.layer.cornerRadius = 2;
    button.clipsToBounds = YES;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.action = action;
    [self addSubview:button];
    return button;
}

/**
 *  弹窗按钮360
 *
 *  @param frame
 *  @param title
 *
 *  @return button
 */
- (UIButton *)XCAlertButtonWithFrame:(CGRect)frame title:(NSString *)title action:(void (^)(UIButton *))action{
    
    GDButton *button = [GDButton buttonWithType:UIButtonTypeCustom];
    if (title != nil) {
        [button setTitle:title forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:15];
    }
    button.frame = CGRectMake(frame.origin.x, frame.origin.y, 360/2, 90/2);
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x3b5286, 1)] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[SingleOperationManager createImageWithColor:XUIColor(0x3b5286, 0.75)] forState:UIControlStateHighlighted];
    button.layer.cornerRadius = 2;
    button.clipsToBounds = YES;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.action = action;
    [self addSubview:button];
    return button;

}
#endif

@end
