//
//  UIView+GDExtension.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GDExtension)
@property (assign, nonatomic) CGFloat x;
@property (assign, nonatomic) CGFloat y;
@property (assign, nonatomic) CGFloat w;
@property (assign, nonatomic) CGFloat h;
@property (assign, nonatomic) CGSize size;
@property (assign, nonatomic) CGPoint origin;
@property (assign, nonatomic) CGFloat max_Y;
@property (nonatomic, assign) CGFloat max_X;

@end
