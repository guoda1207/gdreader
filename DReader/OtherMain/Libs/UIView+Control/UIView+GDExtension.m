//
//  UIView+GDExtension.m
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "UIView+GDExtension.h"

@implementation UIView (GDExtension)
- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}
- (CGFloat)max_X {
    return self.x + self.w;
}
- (void)setMax_X:(CGFloat)max_X {
    self.max_X = max_X;
}
- (CGFloat)max_Y {
    return self.y + self.h;
}
- (void)setMax_Y:(CGFloat)max_Y {
    self.max_Y = max_Y;
}
- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setW:(CGFloat)w
{
    CGRect frame = self.frame;
    frame.size.width = w;
    self.frame = frame;
}

- (CGFloat)w
{
    return self.frame.size.width;
}

- (void)setH:(CGFloat)h
{
    CGRect frame = self.frame;
    frame.size.height = h;
    self.frame = frame;
}

- (CGFloat)h
{
    return self.frame.size.height;
}

- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin
{
    return self.frame.origin;
}



@end
