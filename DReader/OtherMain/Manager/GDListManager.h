//
//  GDListManager.h
//  DReader
//
//  Created by moqing on 2017/9/5.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    CenterPushType_Login,
    CenterPushType_person,
    CenterPushType_coin,
} CenterPushType;

@interface GDListManager : NSObject


@property (nonatomic, copy) void(^listManagerBlock)(CenterPushType type);

+ (instancetype)shareInstance;

- (void)pushViewController:(CenterPushType)type rootNavigation:(UINavigationController*)nav;

@end
