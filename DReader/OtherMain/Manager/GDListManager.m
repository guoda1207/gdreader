//
//  GDListManager.m
//  DReader
//
//  Created by moqing on 2017/9/5.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import "GDListManager.h"
#import "GDHeader.h"
#import "GDLoginVC.h"
#import "GDSideViewController.h"
@interface GDListManager ()


@end

@implementation GDListManager

+ (instancetype)shareInstance {
    
    static GDListManager *manager = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void)pushViewController:(CenterPushType )type rootNavigation:(UINavigationController *)nav{
    UIViewController *vc = [[UIViewController alloc] init];
    switch (type) {
        case CenterPushType_Login:
        {
            vc = [[GDLoginVC alloc] init];
            
        }
            break;
            
        default:
            break;
        
    }
    vc.hidesBottomBarWhenPushed = YES;
    [nav pushViewController:vc animated:YES];
//    vc. = YES;
//    GDSideViewController *side = AppSideVC;
//    [side hideSideViewController:YES];
}


@end
