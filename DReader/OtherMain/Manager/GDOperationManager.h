//
//  GDOperationManager.h
//  DReader
//
//  Created by moqing on 2017/9/4.
//  Copyright © 2017年 guoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//FOUNDATION_EXPORT NSString *const foundHttpURL;

FOUNDATION_EXPORT NSString *const baseURL;

FOUNDATION_EXPORT NSString *const BASEHTTPURL;

FOUNDATION_EXPORT NSString *const BASESHAREDHTTPURL;

//NSString *const JSBRIDGEHEADER = @"mqappjsbridge://";


@interface GDOperationManager : NSObject

+ (instancetype)shareInstance;

+ (UIImage*) createImageWithColor: (UIColor*) color;

+ (NSString*)getCurrentVersion;

+ (NSString*)deviceString;

#pragma mark - 打印json中的key values
- (void)logPropertyList:(NSDictionary*)dictionary;


@end
